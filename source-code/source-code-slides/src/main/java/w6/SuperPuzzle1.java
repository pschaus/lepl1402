package w6;

public class SuperPuzzle1 {

    public static void main(String args[]) {
        D d= new D();
        d.display();
    }

    static class A {
        void foo() {
            System.out.println("foo A");
        }
    }
    static class B extends A {
        void foo() {
            System.out.println("foo B");
        }
    }
    static class C extends B {

    }
    static class D extends C {
        void foo() {
            System.out.println("foo D");
        }
        void display() {
            super.foo();
        }
    }
}

