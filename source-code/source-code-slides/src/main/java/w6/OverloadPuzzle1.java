package w6;

public class OverloadPuzzle1 {


    public static void main(String[] args) {
        A d = new D();
        System.out.println(d.getClass());
        foo(d);
    }



    public static void foo(A a) {
        System.out.println("foo a");
    }
    public static void foo(B b) {
        System.out.println("foo b");
    }
    public static void foo(D d) {
        System.out.println("foo d");
    }



    static class A { }
    static class B extends A { }
    static class C extends B { }
    static class D extends C { }
    static class Test {


    }
}


