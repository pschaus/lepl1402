package w6questions;

public class InnerClasses {

    static int v;

    public InnerClasses(int v) {
        NonStaticInnerClass inner = new NonStaticInnerClass();
        System.out.println(inner);

    }


    class NonStaticInnerClass {
        @Override
        public String toString() {
            return v+"";
        }

    }

    static class StaticInnerClass {
        @Override
        public String toString() {
            return v+"";
        }

    }

    public static void main(String[] args) {
        new InnerClasses(6);
    }


}

class OuterClass {
    static int a = InnerClasses.v;
}
