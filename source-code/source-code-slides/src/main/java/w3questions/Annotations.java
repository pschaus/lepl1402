package w3questions;

public class Annotations {

    public static void main(String[] args) {

        Car redCar = new Car("red");
        Car blueCar = new Car("red");
        System.out.println(redCar.equals(blueCar));

    }



}

class Car {
    String color;

    public Car(String c) {
        this.color = c;
    }


    public boolean equals(Object obj) {
        Car other = (Car) obj;
        return color.equals(other.color);
    }
}
