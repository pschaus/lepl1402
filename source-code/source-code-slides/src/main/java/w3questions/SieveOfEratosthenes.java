package w3questions;

import java.util.BitSet;

public class SieveOfEratosthenes {

    public static void main(String[] args) {

        int a = 300; // 100101100
        byte b = (byte)a; // // 00101100 = 2^2+2^3+2^5 = 4+8+32
        System.out.println(b);


        System.out.println(numberOfPrime(5)); // 2,3,5
    }

    public static int numberOfPrime(int n){
        BitSet primes = new BitSet(n+1);
        primes.set(2,n+1);
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (primes.get(i)) {
                for (int j = i*i; j <= n; j+= i) {
                    primes.clear(j);
                }
            }
        }
        return primes.cardinality();


        //TODO By Student
    }
}
