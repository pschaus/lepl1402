package w3questions;

public class DangerousCode {

    public static void main(String[] args) {

        try {
            dangerousMethod1();
        } catch (MonException e) {

        } catch (Exception e){

        }
        dangerousMethod2(); // pas besoin de faire un try catch

    }

    public static void dangerousMethod1() throws MonException  { // je suis obligé de déclarer dans la signature
        int a;
    }

    public static void dangerousMethod2() { // je ne dois pas déclarer dans la signature
        try {
            throw new MonException(); // dangerous line
        } catch (Exception e) {

        }
    }

}

class MonException extends Exception {

}
