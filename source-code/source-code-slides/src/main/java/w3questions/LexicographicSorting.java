package w3questions;

import java.time.DayOfWeek;
import java.util.Arrays;

public class LexicographicSorting {

    public static void main(String[] args) {

        Pair<Long, String>[] tab1 = new Pair[]{new Pair(3L, "Peter"), new Pair(3L, "Siegfried"), new Pair(3L, "Sofie"), new Pair(3L, "Olivier"), new Pair(3L, "Pierre")};

        Arrays.sort(tab1);

        Pair<DayOfWeek, Integer>[] tab2 = new Pair[]{new Pair(DayOfWeek.FRIDAY, 2), new Pair(DayOfWeek.MONDAY, 3), new Pair(DayOfWeek.FRIDAY, 4)};
        Arrays.sort(tab2);
    }
}


class Pair<K extends Comparable,V extends Comparable> implements Comparable {
    K first;
    V second;

    public Pair(K f, V second) {
        this.first = f;
        this.second = second;
    }

    @Override
    public int compareTo(Object o) {
        Pair<K,V> other = (Pair<K,V>) o;
        if (first.compareTo(other.first) < 0) {
            return -1;
        } else if (first.compareTo(other.first) > 0) {
            return +1;
        } else {
            return second.compareTo(other.second);
        }
    }
}
