package w3questions;

import java.util.Iterator;

public class MyIterableCollection implements Iterable<Integer> {

    private final int[] array;
    int size = 0;

    public MyIterableCollection(int capacity) {
        array = new int[capacity];
    }

    public void add(int v) {
        array[size] = v;
        size++;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new MyIterator();
    }

    class MyIterator implements Iterator<Integer> {

        int index = 0;

        @Override
        public Integer next() {
            int result = array[index];
            index++;
            return  result;
        }

        @Override
        public boolean hasNext() {
            return index < size;
        }
    }

    public static void main(String[] args) {
        MyIterableCollection array = new MyIterableCollection(10);
        array.add(2);
        array.add(3);
        for (int v1: array) {
            for (int v2: array) {
                System.out.println(v1+" "+v2);
            }
            //System.out.println(v);
        }
    }

}
