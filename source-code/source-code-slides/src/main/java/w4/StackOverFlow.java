package w4;

public class StackOverFlow {
    public static void main(String[] args) {
        System.out.println(factorial(100000));
    }

    public static long factorial(int n) {
        if (n <= 2) {
            return n;
        }
        return n * factorial(n - 1);
    }
}
