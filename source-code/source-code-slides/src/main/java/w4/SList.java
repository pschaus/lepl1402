package w4;

import java.util.*;



public class SList<T> implements Iterable<T> {

    private class SListNode {
        T item;
        public SListNode next;

        private SListNode(T i, SListNode n) {
            item = i;
            next = n;
        }
    }

    // Class invariant:
    // size is the number of elements in the list formed
    // by the chain of the next pointers starting at head
    // and finishing with a null reference:
    // head -> head.next -> head.next.next ... -> null
    // if size == 0 then head == null

    private SListNode head;
    private int size;

    public void insertFront(T item) {
        head = new SListNode(item, head);
        size++;
    }

    public int size() {
        return size;
    }

    public Iterator<T> iterator() {
        return new SListIterator(this);
    }



    private class SListIterator implements Iterator<T> {
        private SListNode n;

        private SListIterator(SList l) {
            n = l.head;
        }

        public boolean hasNext() {
            return n != null;
        }

        public T next() {
            if (n == null) {
                throw new NoSuchElementException();
            }
            T i = n.item;
            n = n.next;
            return i;
        }

        public void remove() {
            throw new UnsupportedOperationException("not implemented");
        }
    }

    public static void main(String[] args) {
        SList<Integer> l = new SList();


        l.insertFront(3);
        l.insertFront(2);

        for (int o : l) {
            System.out.println(o);
        }

        System.out.println("size"+l.size());



    }
}