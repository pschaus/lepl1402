package w1;

class Car {


    String color;
    int speed = 0;
    int acceleration = 0;

    Car(String color, int acceleration) {
        this.color = color;
        this.acceleration = acceleration;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof  Car) {
            Car c1 = (Car) obj;
            return color.equals(c1.color) && speed == c1.speed;
        }
        return false;
    }

    public static void main(String[] args) {
        Car c1 = new Car("black",2);

        //Car c1 = new Car("black",5);
        //Car c2 = new Car("black",5);

        //System.out.println(c1.equals(c2)); // true
    }

}