package w4questions;

public class Complexity {

    // N  Time(s)
    // 100    5
    // 200   20
    // 400   80
    // 800  320

    // Theta(N^2) = 5
    // Theta((2N)^2) = 4 Theta(N^2)

    //

    // What could be the complexity of the algorithm ?

    public static void main(String[] args) {

        for(int n = 1; n < 100000; n*=2) {
            System.out.println(n+" "+algo3(n));
        }
    }

    // Theta(n)
    public static void algo0(int n) {
        int k = 2;
        for (int i = 0; i < n; i++) {
            i += k;
            System.out.println(i);
        }
    }

    public int algo1(int n) {
        int value = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i; i++) {
                value += 1;
            }
        }
        return value;
    }

    public static int algo2(int n) {
        int cpt = 0;
        int i = n;
        while (i > 0) {
            cpt += 1;
            i = i / 2;
        }
        return cpt;
    }



    public static int algo3(int n) {
        int a = 0;
        int i = n;
        // O(n)*Theta(log(n)) = O(n*log(n))

        // va s'executer Theta(log(n))
        while (i > 0) {
            i = i / 2;
            for (int j = 0; j < i; j++) {
                a++;
            }
        }
        return a;
    }

    /*
     *    Given the array
     *     [-2,1, -3, 4,-1,2,1,-5,4]
     *     [-2,-1,-4,-0,-1,1,2,-3,1]
     *       0  1  2  3 4  5 6, 7,8]
     *    The contiguous subarray that produces the best result is [4,-1,2,1]
     *    For this array your method should return [6, 3, 6]
     */
    public static int[] maxSubArray(int[] a){
        // Theta(n)
        int [] prefixSum = new int[a.length];
        prefixSum[0] = a[0];
        // Theta(n)
        for (int i = 1; i < a.length; i++) {
            prefixSum[i] = prefixSum[i-1]+a[i];
        }

        int i_  = 0;
        int j_ = 0;
        int best = a[i_];
        // Theta(n^2)
        for (int i = 0; i < a.length; i++) {
            for (int j = i; j < a.length; j++) {

                // sum a[i..j] O(1)
                int sum = prefixSum[j]-prefixSum[i-1];

            }
        }

        return new int[]{};
        //TODO By Student
    }




}
