package w4questions;

public class MergeSort {

    /**
     * Pre-conditions: a[lo..mid] and a[mid+1..hi] are sorted
     * Post-conditions: aux[lo..hi] is sorted
     */
    public static void merge(int[] a, int[] aux, int lo, int mid, int hi) {
        // 1,2,3,4,5
        int j = lo;
        int k = mid+1; // 3
        int i = lo; // 0
        while (i <= hi) {
            if ((k > hi || a[j] < a[k]) && j <= mid) {
                aux[i] = a[j];
                j++;
            } else { // j > mid || (k <= hi && a[k] <= a[j])
                aux[i] = a[k];
                k++;
            }
            i++;
        }

        while(j <= mid && k <= hi) {
            if(a[j] < a[k]) {
                aux[i] = a[j];
                j++;
            }
            else {
                aux[i] = a[k];
                k++;
            }
        }
        while(j <= mid) {
            aux[i] = a[j];
            j++; i++;
        }
        while(k <= hi) {
            aux[i] = a[k];
            k++; i++;
        }

        // TODO By Student
    }
    /**
     * Rearranges the array in ascending order, using the natural order
     */
    public static void sort(int[] a) {
        int [] aux = new int[a.length];
        mergeSort(a,aux,0,a.length-1);
        // TODO By Student
    }

    /**
     *
     * @param a
     * @param aux
     * @param lo
     * @param hi
     */
    public static void mergeSort(int a[], int [] aux,int lo, int hi) {
        if (lo < hi) {
            int mid = lo + (hi-lo)/2;
            mergeSort(a,aux,lo,mid);
            mergeSort(a,aux,mid+1,hi);
            merge(a,aux,lo,mid,hi);
            for (int i = lo; i <= hi; i++) {
                a[i] = aux[i];
            }
        }
    }

    //TODO Optionnal additionnal method


}
