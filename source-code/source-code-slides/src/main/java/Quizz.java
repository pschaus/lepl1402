import java.util.Arrays;

public class Quizz {


    /**
     *
     * bi-partition the sub-array[from..to-1] in two:
     * array[from..i-1] =
     */
    public static int partition(int[] array, int from, int to, int value) {
        int last = from;
        for (int i = from; i < to; i++) {
            if (array[i] == value) {
                int tmp = array[last];
                array[last] = array[i];
                array[i] = tmp;
                last++;
            }
        }
        return last;
    }

    public static void flagPartition(int[] array) {
        int i = partition(array,0,array.length,0);
        System.out.println(Arrays.toString(array));
        System.out.println(i);
        i = partition(array,i,array.length,1);
    }

    public static void main(String[] args) {
        int [] test = new int[] {0,1,2,1,1,2,1,0,2,1,0};
        flagPartition(test);
        System.out.println(Arrays.toString(test));
    }

}
