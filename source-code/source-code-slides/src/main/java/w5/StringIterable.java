package w5;

import java.util.ArrayList;
import java.util.Iterator;

public class StringIterable implements Iterable<Character> {
    static int nInstanceIterator =  0;

    String s;
    public StringIterable(String s) {
        this.s = s;
    }

    @Override
    public Iterator<Character> iterator() {
        nInstanceIterator++;
        return new Iterator<Character>() {
            int index = 0;

            @Override
            public boolean hasNext() {
                return index < s.length();
            }

            @Override
            public Character next() {
                return s.charAt(index++);
            }
        };
    }

    private class MyIterator implements Iterator<Character> {

        int index = 0;


        @Override
        public boolean hasNext() {
            return index < s.length();
        }

        @Override
        public Character next() {
            char c = s.charAt(index);
            index += 1;
            return c;
        }
    }


    public static void main(String[] args) {
        String s = "toto";
        StringIterable totoIterable = new StringIterable(s);
        for (char c1: totoIterable) {
            /*
            Iterator<Character> ite = totoIterable.iterator();
            while (ite.hasNext()) {
                System.out.println(c1+" "+ite.next());
            }*/

            for (char c2: totoIterable) {
                System.out.println(c1+" "+c2);
            }

        }
        System.out.println("nombre d'iterrator:"+StringIterable.nInstanceIterator);

    }


}
