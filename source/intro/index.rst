	.. _intro:


************
Organization
************


Pedagogy
=======================================



We will use *Java* and more specifically Java8_.
The recommended IDE is IntelliJ_.
We will use Junit4_ to perform unit tests.
It is highly recommended to become familiar with these tools to get ready for the exam.

.. _Java8: https://docs.oracle.com/javase/8/docs/api.
.. _IntelliJ: https://www.jetbrains.com/idea/
.. _Inginious: https://inginious.info.ucl.ac.be
.. _JUnit4: https://junit.org/junit4/.


The lectures will be delivered through videos exclusively and the exercise sessions will be organized on teams (individually on Inginious).

Starting in week 2, the students who have the permission, can come for a Q&A session in A10 at 16h15.
This session will be recorded as well so that the students who cannot attend are not penalized.


Course Open-Source
=======================================

This website and the teaching material it contains is open-source `bitbucket <https://bitbucket.org/pschaus/lepl1402/src>`_.
We welcome pull requests to fix errors or proposing new exercises.
The license is Creative Commons Attribution-ShareAlike 4.0 International License:

.. image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
    :alt: CC-BY-SA


Agenda
=======================================

The course is organized in 6 modules of 2 weeks each.

* Thursday w1: video lecture by Prof. P. Schaus (modules 1-3) or Prof. Ramin Sadre (modules 4-6)
* w1: preparation of exercises (TA can help on teams on Wednessday 8h30-10h30 and 14h00-16h00)
* Thursday w2: video lecture by Prof. P. Schaus (modules 1-3) or Prof. Ramin Sadre (modules 4-6)
* w2: preparation of exercises (TA can help on teams on Wednessday 8h30-10h30 and 14h00-16h00)

Starting in w2, an interactive Q&A session with the professor is organised at 16h10 auditorium A10.


Evaluation
=======================================

Exam on Inginious + One mid-term quizz during the semester.

An optional quiz takes place at mid-term counting for the continuous assessment only if the mark is higher than the exam mark.
The objective of the exam at the end of the term is to check not only knowledge of the subject, but also the ability to apply the knowledge acquired to write programs. 
The exam and quiz will use the Inginious grading system. 
In case of doubt, teachers reserve the right to have certain students take an additional hybrid exam that supplements or replaces the grade obtained on Inginious.

Warning: any case that is detected as plagiarism by our tools will be transferred to the president of the jury. 
The students cannot collaborate (in any form) and use external source of information during the quizz and exams except the java-api.
(and the slides of the course for the quizz).


Contact et communication
=======================================


Important communications using moodle.
Also check this page on a regular bases.
For important matters:
'Sébastien Jodogne <sebastien.jodogne@uclouvain.be>`_,
`Ramin Sadre <ramin.sadre@uclouvain.be>`_. and
`Pierre Schaus <pierre.schaus@uclouvain.be>`_ 
