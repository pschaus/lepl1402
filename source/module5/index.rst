.. _module5:


*************************************************************************************************
Module 5 | Functional Programming and Lambda expressions
*************************************************************************************************

Objective
=========

By the end of this module, students will be able to:

* Use inner classes
* Use functional interfaces and lambda expressions
* Use functional concepts, such as immutable lists and streams in Java

Resources: Part 1 (Lambda expressions, immutable lists)
=======================================================

Slides

* :download:`Inner classes (PDF file) <./part1_slides/1_innerclass.pdf>`
* :download:`Functional programming, part 1 (PDF file) <./part1_slides/2_functions.pdf>`
* :download:`Summary of inner classes and lambda functions (PDF file) <./part1_slides/2022-12-01-Informatics2-LambdaSummary.pdf>`

Youtube playlist:

* `Lecture 5a (Playlist) <https://www.youtube.com/playlist?list=PLBMhFQpVgBPn6EE4e_gJTeshN4xjGLMQE>`_

Java code examples (right-click to download):

* :download:`Example code: inner and anonymous inner classes (zip file) <./part1_code/module5_innerclass.zip>`
* :download:`Example code: lambda expressions and Function interface (java source) <./part1_code/LambdaTest.java>`
* :download:`Example code: sorting ducks (java source) <./part1_code/SortingDucks.java>`

Exercises:

* `Lambda expressions <https://inginious.info.ucl.ac.be/course/LEPL1402/LambdaExpressioninJava>`_
* `Functional list <https://inginious.info.ucl.ac.be/course/LEPL1402/Generics>`_
* `Functional list with generics <https://inginious.info.ucl.ac.be/course/LEPL1402/Generics2>`_
* `Logical Circuit with functions <https://inginious.info.ucl.ac.be/course/LEPL1402/Generics3>`_

Resources: Part 2 (immutable lists part 2, streams, more on lambda expressions)
================================================================================

Slides

* :download:`Functional programming, part 2 (PDF file) <./part2_slides/1_functions_part2.pdf>`
* :download:`Streams (PDF file) <./part2_slides/2_streams.pdf>`
* :download:`Optional (PDF file) <./part2_slides/3_optional.pdf>`

Youtube playlist:

* `Lecture 5b (Playlist) <https://www.youtube.com/playlist?list=PLBMhFQpVgBPk4J--4yYAW0o9IduU96wfp>`_

Exercises:

* `FList with Cons and Nil <https://inginious.info.ucl.ac.be/course/LEPL1402/FList>`_
* `MergeSort for FList <https://inginious.info.ucl.ac.be/course/LEPL1402/FListMergeSort>`_
* `Functional immutable tree <https://inginious.info.ucl.ac.be/course/LEPL1402/FTree>`_
* `Streams <https://inginious.info.ucl.ac.be/course/LEPL1402/Streams>`_
* `More streams <https://inginious.info.ucl.ac.be/course/LEPL1402/Streams2>`_
* `Optional <https://inginious.info.ucl.ac.be/course/LEPL1402/Optional>`_
