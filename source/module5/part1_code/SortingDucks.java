package live;

import java.util.Arrays;
import java.util.Comparator;

class Duck {
    int weight;

    public Duck(int weight) {
        this.weight = weight;
    }
}


public class SortingDucks {

    public void run() {
        Duck duck1 = new Duck(10);
        Duck duck2 = new Duck(20);
        Duck duck3 = new Duck(15);

        Duck[] ducks = new Duck[] { duck1, duck2, duck3 };
        Arrays.sort(ducks, (o1,o2) -> o2.weight-o1.weight );
    }
}
