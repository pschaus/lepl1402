package live;

import java.util.function.*;

interface MyFunction {
    public double calculate(double d);
}

interface MyFunctionWithTwoParameters {
    public String calculate(int p1, double p2);
}

public class LambdaTest {

    public double timesTwo(MyFunction function, double d) {
        return function.calculate(d)*2.0;
    }

    public void run() {
        double result=timesTwo((d) -> Math.sqrt(d), 10.0);

        double result2=timesTwo((d) -> d+2.0, 10.0);

        Function<Double, Double> f1=(d) -> Math.sqrt(d);
        Function<Double, Double> f2=(d) -> d+2.0;
        double result3=f2.apply(f1.apply(10.0));

        Function<Double,Double> f3 = f2.compose(f1);
        double result4=f3.apply(10.0);

        BiFunction<Integer,Double,String> f4 = (p1,p2) -> {
          if(p1<p2) {
              return "Hello";
          }
          else {
              return "World";
          }
        };
        String result5 = f4.apply(4, 5.0);
    }
}
