public abstract class DecisionTree {

    public abstract boolean predict(boolean[] features);

    public static DecisionTree splitNode(int featureIndex, DecisionTree left, DecisionTree right) {
        return new TestNode(featureIndex, left, right);
    }

    public static DecisionTree decisionNode(boolean label) {
        return new Diagnostic(label);
    }

    private static class TestNode extends DecisionTree {
        int featureIndex;
        DecisionTree left;
        DecisionTree right;

        TestNode(int featureIndex, DecisionTree left, DecisionTree right) {
            this.featureIndex = featureIndex;
            this.left = left;
            this.right = right;
        }

        public boolean predict(boolean[] features) {
            if (features[featureIndex]) {
                return left.predict(features);
            } else {
                return right.predict(features);
            }
        }
    }

    private static class Diagnostic extends DecisionTree {
        boolean answer;

        Diagnostic(boolean answer) {
            this.answer = answer;
        }

        public boolean predict(boolean[] features) {
            return answer;
        }
    }
}
