public class Tree {

    public Node root;

    public Tree(Node root) {
        this.root = root;
    }

    @Override
    public boolean equals(Object o) {
        // YOUR CODE HERE
        if (o != null &&
                o instanceof Tree) {
            Tree tree = (Tree) o;

            if (root == null) {
                return tree.root == null;
            } else if (tree.root == null) {
                return false;
            } else {
                return root.equals(tree.root);
            }
        } else {
            return false;
        }
    }

}