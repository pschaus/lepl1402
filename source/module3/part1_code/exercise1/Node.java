public class Node {

    public int val;
    public Node left;
    public Node right;

    public Node(int val) {
        this.val = val;
    }

    public boolean isLeaf() {
        return this.left == null && this.right == null;
    }

    @Override
    public boolean equals(Object o) {
        // YOUR CODE HERE
        if (o == null ||
                !(o instanceof Node)) {
            return false;
        }

        Node other = (Node) o;

        if (isLeaf() && other.isLeaf()) {
            // COMPARE LEAVES
            return val == other.val;
        } else if (!isLeaf() && !other.isLeaf()) {
            // COMPARE INTERNAL NODES
            if (left == null) {
                return other.left == null;
            } else if (other.left == null ||
                    !(left.equals(other.left))) {
                return false;
            }

            if (right == null) {
                return other.right == null;
            } else if (other.right == null ||
                    !(right.equals(other.right))) {
                return false;
            }

            // HERE: "left" and "right" are the same for both nodes!
            return true;
        } else {
            return false;
        }
    }
}