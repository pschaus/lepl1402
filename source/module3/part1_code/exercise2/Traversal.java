import java.util.List;
import java.util.Stack; // this should give you a hint for the iterative version

public class Traversal {

    public static void recursiveInorder(Node root, List<Integer> res) {
        // YOUR CODE HERE

        // "PRINT LEFT"
        if (root.left != null) {
            recursiveInorder(root.left, res);
        }
        res.add(root.val);

        // "PRINT RIGHT"
        if (root.right != null) {
            recursiveInorder(root.right, res);
        }
    }


    public static void iterativeInorder(Node root, List<Integer> res) {
        // YOUR CODE HERE

        Stack stack = new Stack();

        Node current = root;
        while (current != null) {
            stack.add(current);
            current = current.left;
        }

        while (!stack.empty()) {
            current = (Node) stack.pop();

            res.add(current.val);

            current = current.right;
            while (current != null) {
                stack.add(current);
                current = current.left;
            }
        }
    }

}
