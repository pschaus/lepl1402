.. _module4:

*************************************************************************************************
Module 4 | Software Engineering Topics: Testing, Design patterns
*************************************************************************************************

Objective
=========

* Understanding control flows
* Designing test cases and how to use code coverage
* Design patterns

Resources: Part 1 (Testing)
=======================================

Slides

* :download:`Software testing (PDF file) <./part1_slides/1_software_testing.pdf>`
* :download:`Automated testing (PDF file) <./part1_slides/2_automated_testing.pdf>`
* :download:`Test coverage (PDF file) <./part1_slides/3_test_coverage.pdf>`
* :download:`Practical aspects (PDF file) <./part1_slides/4_practical_aspects.pdf>`

Youtube playlist:

* `Lecture 4a - Testing (Playlist) <https://www.youtube.com/playlist?list=PLBMhFQpVgBPknBcse6BrLFaiaYstdfpG->`_

Java code examples (right-click to download):

* :download:`Example code: min method (java source) <./part1_code/module4_min/Main.java>`
* :download:`Example code: test code for min method without JUnit (java source) <./part1_code/module4_min/MainTest_manually.java>`
* :download:`Example code: test code for min method with JUnit (java source) <./part1_code/module4_min/MainTest_junit.java>`

Exercises:

* `Blackbox testing <https://inginious.info.ucl.ac.be/course/LEPL1402/BlackBox>`_
* `CFG basics <https://inginious.info.ucl.ac.be/course/LEPL1402/CFGBasic>`_
* `Coverage testing basics <https://inginious.info.ucl.ac.be/course/LEPL1402/CoverageBasic>`_
* `Coverage testing intermediate <https://inginious.info.ucl.ac.be/course/LEPL1402/CoverageIntermediate>`_
* `Coverage testing advanced <https://inginious.info.ucl.ac.be/course/LEPL1402/Coverage>`_

Resources: Part 2 (Design patterns)
=======================================

Slides:

* :download:`Singleton (PDF file) <./part2_slides/1_singleton.pdf>`
* :download:`Factory method (PDF file) <./part2_slides/2_factory_method.pdf>`
* :download:`Observer/Observable (PDF file) <./part2_slides/3_observer.pdf>`
* :download:`Visitor (PDF file) <./part2_slides/4_visitor.pdf>`

Youtube playlist:

* `Lecture 4b - Design patterns (Playlist) <https://www.youtube.com/playlist?list=PLBMhFQpVgBPlQGLicbIRrd45_x7jscum1>`_

Java code examples:

* :download:`Example code: singleton (zip file) <./part2_code/module4_singleton.zip>`
* :download:`Example code: factory (zip file) <./part2_code/module4_factory.zip>`
* :download:`Example code: observer (zip file) <./part2_code/module4_observer.zip>`
* :download:`Example code: visitor (zip file) <./part2_code/module4_visitor.zip>`

Exercises:

* `Factory design pattern <https://inginious.info.ucl.ac.be/course/LEPL1402/Factory>`_
* `Observer design pattern <https://inginious.info.ucl.ac.be/course/LEPL1402/Observer>`_
* `Visitor design pattern basics: list filtering <https://inginious.info.ucl.ac.be/course/LEPL1402/VisitorBasic>`_
* `Visitor design pattern: staff statistics <https://inginious.info.ucl.ac.be/course/LEPL1402/StaffVisitor>`_

  
Restructuration 2021
--------------------

* :download:`Slides <./2021-11-18-Restructuration/2021-11-18-Restructuration.pdf>`
* Versions 1 and 2 of the "find pattern" quiz: :download:`FindPattern.java <./2021-11-18-Restructuration/FindPattern-v1-v2.java>`
* Version 3 of the "find pattern" quiz: :download:`FindPattern.java <./2021-11-18-Restructuration/FindPattern-v3.java>`
* Source code of the "merge linked lists without duplicates" quiz: :download:`MergeSortedLinkedListDuplicate.java <./2021-11-18-Restructuration/MergeSortedLinkedListDuplicate.java>`
