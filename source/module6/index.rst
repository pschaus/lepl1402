.. _module6:


*************************************************************************************************
Module 6 | Parallelism: threads and introduction to synchronisation
*************************************************************************************************

Objective
=========

By the end of this module, students will be able to:

* Use threads
* Use mechanisms (monitors, locks,...) to synchronize threads
* Use basic thread-safe data structures

Resources: Part 1 (Threads, Monitors, Wait and Notify)
=======================================================

Slides

* :download:`Threads and thread pools (PDF file) <./part1_slides/1_threads.pdf>`
* :download:`Race conditions and monitors (PDF file) <./part1_slides/2_monitors.pdf>`
* :download:`wait() and notify() (PDF file) <./part1_slides/3_wait_notify.pdf>`

Youtube playlist:

* `Lecture 6a (Playlist) <https://www.youtube.com/playlist?list=PLBMhFQpVgBPmyljaxcy3HZNeP1wAXair7>`_

Java code examples:

* :download:`All examples in one zip file <./part1_code/module6_part1.zip>`

Exercises:

* `Small exercise on how to create threads <https://inginious.info.ucl.ac.be/course/LEPL1402/ThreadsIntroduction>`_
* `Using futures <https://inginious.info.ucl.ac.be/course/LEPL1402/Future>`_
* `Implementing a counter with monitors [medium] <https://inginious.info.ucl.ac.be/course/LEPL1402/SharedCounter>`_
* `Producers and Consumers with bounded buffer [hard] <https://inginious.info.ucl.ac.be/course/LEPL1402/BoundedBuffer>`_

Resources: Part 2 (More about threads)
=======================================================

Slides

* :download:`Visibility, deadlocks (PDF file) <./part2_slides/1_visibility_deadlocks.pdf>`
* :download:`Locks (PDF file) <./part2_slides/2_locks.pdf>`
* :download:`More useful tools to work with threads (PDF file) <./part2_slides/3_more_tools.pdf>`

Youtube playlist:

* `Lecture 6b (Playlist) <https://www.youtube.com/playlist?list=PLBMhFQpVgBPlrh60cXaF6g3N-pX7wKrb2>`_

Exercises:

* `Producers/Consumers with locks [medium] <https://inginious.info.ucl.ac.be/course/LEPL1402/ProducerConsumer>`_
* `CyclicBarrier [hard] <https://inginious.info.ucl.ac.be/course/LEPL1402/CyclicBarrier>`_
* `Merge Sort with ForkJoinPool [hard] <https://inginious.info.ucl.ac.be/course/LEPL1402/ParallelelMergeSort>`_




